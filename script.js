/*ТЕОРІЯ

1. Оператори в JavaScript - це символи або ключові слова, що використовуються для виконання операцій над змінними чи значеннями. 
Типи операторів включають арифметичні, порівняння, логічні, присвоєння . Оператори за кількістю значень поділяються на дві групи:
унарні - це оператори, які потребують одного операнда,
бінарні - ці оператори використовують два операнди.


2. Оператори порівняння використовуються для порівняння значень та визначення, чи є вони рівними, меншими або більшими. Наприклад:

== - рівність (порівнює значення, без перевірки типу);
=== - строга рівність (порівнює значення та типи);
!= - нерівність (перевіряє, чи не рівні значення, без перевірки типу);
!== - строга нерівність (перевіряє, чи не рівні значення або типи).

3.Операції присвоєння в JavaScript використовуються для присвоєння значення змінній. Деякі приклади операцій присвоєння:

= - просте присвоєння;
+= - додати та присвоїти (наприклад, x += 5 еквівалентно x = x + 5);
-= - відняти та присвоїти (наприклад, y -= 3 еквівалентно y = y - 3);
*= - помножити та присвоїти (наприклад, z *= 2 еквівалентно z = z * 2).

ПРАКТИКА
*/
'use strict'

//1.

const username = 'Timur';
const password = 'secret';

let signIn = prompt(username);
console.log(password == signIn);

// 2. 

const x = 5;
const y = 3;

alert(x+y);
alert(x-y);
alert(x*y);
alert(x/y);
